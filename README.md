# Build and Run
```bash
docker build -t name_of_image .
docker run -d -e "PORT=5000" -p 5000:5000 --name name_of_container name_of_image
```
Opening "localhost:5000" on browser will show a message.

# Fail and Success
To make **fail** commit, open "test/jest/app/app.js" and give different value in "expect" and "toEqual". To make **success** commit, do otherwise.