FROM node:9

WORKDIR /usr/src/app
COPY . .
RUN npm install

CMD [ "node", "app.js" ]